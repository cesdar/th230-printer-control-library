# TH230 printer control Library

Simple library for controlling the Wincor Nixdorf TH230 or TH230+ POS receipt printer.

It is wrtitten in C++ with easy portability to other platforms in mind. The main class can be found in the /src folder and platform specific implementations are located in /platform. Testing code can be found in the folder /test.
