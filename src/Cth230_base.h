#ifndef CTH230_BASE_HPP
#define CTH230_BASE_HPP

//This class implements the programming guide of the TH230 thermal receipt printer by Wincor Nixdorf
//Could be accesed e.g. here: https://www.pwks.de/shop/downloads/th230_programmers_guide_english.pdf

#include <stdint.h>

class Cth230_base
{
public:
    Cth230_base(uint16_t printWidth = 576);
    ~Cth230_base();

    //Printer managing functions
    void powerOff();
    /*
        Sets amount of pixels in one print row
        @param printWidth is width that should be used. 576 or 408 pixel is allowed for TH230
        @return returns selected pixelwidth. Nearest to specified is selected.
    */
    uint16_t setPrintPixelWidth(uint16_t printWidth);
    /*
        Selects double-wide characters. Deactivated after printing of one line or after clearPrintLineBuffer()
    */
    void selectDoubleWideCharacters();
    void selectSingleWideCahracters();
    /*
        **TODO**: Check this beahaviour
        Sets the spacing betwenn two print lines ind pixels/dots. Printer default is 3.
        @param nDotsSapcing specifies the number of dot rows between to tex print lines
    */
    void setLineSpacing(uint8_t nDotsSpacing = 3);
    void pageDeleteData();
    void pagePrint();
    /*
        Executes a full cut of the Paper. Paper falls of printer if angeled rigtt.
    */
    void cutFull();
    /*
        Executes a partial cut. Paper is held losely at one tab togehter
    */
    void cutPartial();
    void bell();
    void rotatePrint90degCounterClockwise();
    void rotatePrintNormal();

    //Printer print functions
    void print(char* text);
    void println(char* text);
    void lineFeed(uint8_t nLinefeeds = 1);
    void formFeed(uint8_t nFormFeeds = 1);
    void clearPrintLineBuffer();
    /*
        Prints one pixel line with pixel being either black or white.
        @param lineData is Array of 8bit data. Each bit encodes a pixel. Has to be 72/51 bytes long for 576/408 pixels printwidth
    */
    void printRasterLine(uint8_t* lineData);
    void feednLines(uint8_t nLines);
    void feednDotRows(uint8_t nRows);
    void setColumn(uint8_t column);

protected:
    virtual int write(char data) = 0;
    virtual int read(char data) = 0;
    virtual int write(char* data, int len) = 0;
    virtual int read(char* data, int len) = 0;

    uint16_t m_printPixelWidth;
};

#endif
