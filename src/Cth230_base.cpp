#include "Cth230_base.h"
Cth230_base::Cth230_base(uint16_t printWidth)
{
    
}

Cth230_base::~Cth230_base()
{
    
}

void Cth230_base::pageDeleteData()
{
    write(0x18);
}

void Cth230_base::pagePrint()
{
    write(0x17);
}

void Cth230_base::cutFull()
{
    write(0x19);
}

void Cth230_base::cutPartial()
{
    write(0x1A);
}

void Cth230_base::bell()
{
    char bell[2] = {0x1B,0x07};
    write(bell,2);
}

void Cth230_base::feednLines(uint8_t nLines)
{
    char cmd[2] = {0x14,0};
    cmd[1] = nLines;
    write(cmd,2);
}

void Cth230_base::feednDotRows(uint8_t nRows)
{
    write(0x15);
    write(nRows);
}
